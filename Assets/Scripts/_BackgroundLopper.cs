﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEditor;
using UnityEngine;

public class BackgroundLopper : MonoBehaviour
{
    public GameObject[] objects;
    private Camera mainCamera;
    public Vector2 screenBounds;
    private bool go = true;

    // Start is called before the first frame update
    void Start()
    {
        this.mainCamera = this.gameObject.GetComponent<Camera>();
        //screen width == camera width?
        this.screenBounds = this.mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, this.mainCamera.transform.position.z));
        this.objects.ToList().ForEach(s => this.FillBackgrounds(s));
    }

    void FillBackgrounds(GameObject obj)
    {
        float objWidth = 0;
        if(obj.GetComponent<SpriteRenderer>() != null)
        {
            objWidth = obj.GetComponent<SpriteRenderer>().bounds.size.x;
        }
        int cloneCount = Math.Max((int)Mathf.Ceil(this.screenBounds.x * 2 / objWidth), 3);
        Debug.LogFormat("FillBackgrounds objWidth[{0}] screenWidth[{1}] repeat[{2}]",
            objWidth, this.screenBounds.x * 2, cloneCount);

        var source = obj;
        Enumerable.Range(0, cloneCount).ToList().ForEach(idx =>
        {
            GameObject c = Instantiate(source) as GameObject;
            c.transform.SetParent(obj.transform);
            c.transform.position = new Vector3(objWidth * idx, obj.transform.position.y, obj.transform.position.z);
            c.name = string.Format("{0}_{1}", obj.name, idx);

            Debug.LogFormat("FillBackgrounds create name[{0}] parent[{1}] index[{2}] x[{3}]",
                c.name, obj.name, idx, c.transform.position.x);

            source = c;
        });
        if (obj.GetComponent<SpriteRenderer>() != null)
        {
            Destroy(obj.GetComponent<SpriteRenderer>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void repositionChildObjects(GameObject obj)
    {
        //var value = false;
        var transforms = obj.GetComponentsInChildren<Transform>().Where(t => t.gameObject != obj).ToList();
        if (!transforms.Any())
            return;

        var first = transforms.First().gameObject;
        var last = transforms.Last().gameObject;
        float halfScreenWidth = this.screenBounds.x;
        float halfObjWidth = last.GetComponent<SpriteRenderer>().bounds.extents.x;
        foreach (var tranform in transforms)
        {
            Debug.Log(string.Format("Child name[{0}]", tranform.name));
        }
        //camera center + camera width
        
        if (this.transform.position.x + halfScreenWidth > last.transform.position.x + halfObjWidth)
        {
            Debug.Log(string.Format("repositionChildObjects -> Last name[{0}] cameraEnd[{1}] firstStart[{2}/{3}] lastEnd[{4}/{5}] half[{6}]",
                obj.name, this.transform.position.x + halfScreenWidth, first.name, first.transform.position.x - halfObjWidth, last.name, last.transform.position.x + halfObjWidth, halfObjWidth));
            first.transform.SetAsLastSibling();
            first.transform.position = new Vector3(last.transform.position.x + halfObjWidth * 2, last.transform.position.y, last.transform.position.z);
            //first = transforms.First().gameObject;
            //last = transforms.Last().gameObject;
            //Debug.Log(string.Format("repositionChildObjects name[{0}] camera[{1}] screen[{2}] first[{3}] last[{4}]",
            //    obj.name, this.transform.position.x, halfScreenWidth, first.transform.position.x, last.transform.position.x));
        }
        else if(this.transform.position.x - halfScreenWidth < first.transform.position.x - halfObjWidth)
        {
            Debug.Log(string.Format("repositionChildObjects -> First name[{0}] camera[{1}] screen[{2}] firstStart[{3}] lastEnd[{4}] half[{5}]",
                last.name, this.transform.position.x, halfScreenWidth, first.transform.position.x - halfObjWidth, last.transform.position.x + halfObjWidth, halfObjWidth));

            last.transform.SetAsFirstSibling();
            last.transform.position = new Vector3(first.transform.position.x - halfObjWidth * 2, first.transform.position.y, first.transform.position.z);
        }
    }

    private void LateUpdate()
    {
        this.objects.ToList().ForEach(s => this.repositionChildObjects(s));
    }
}
