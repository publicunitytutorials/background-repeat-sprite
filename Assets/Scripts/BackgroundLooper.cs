﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BackgroundLooper : MonoBehaviour
{
    public GameObject[] gameObjects;
    private Camera camera;
    Vector2 screenBounds;
    public enum LoopDirection
    {
        Horizental = 0,
        Vertical = 1
    }
    public LoopDirection loopDirection = LoopDirection.Horizental;
    public bool isHorizental
    {
        get
        {
            return this.loopDirection == LoopDirection.Horizental;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        this.camera = this.GetComponent<Camera>();
        this.screenBounds = this.camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, this.camera.transform.position.z));
        this.gameObjects.ToList().ForEach(obj => this.CloneBackgroundObjects(obj));
    }

    void CloneBackgroundObjects(GameObject obj)
    {
        var objSize = this.isHorizental ? obj.GetComponent<Renderer>().bounds.size.x
            : obj.GetComponent<Renderer>().bounds.size.y;
        var cloneCount = this.isHorizental ? (int)Mathf.Ceil(this.screenBounds.x * 2 / objSize) + 2
            : (int)Mathf.Ceil(this.screenBounds.y * 2 / objSize) + 2;
        var source = obj;
        Enumerable.Range(0, cloneCount).ToList().ForEach(idx =>
        {
            var c = Instantiate(source) as GameObject;
            c.transform.SetParent(obj.transform);
            c.transform.position = new Vector3(this.isHorizental ? objSize * idx : obj.transform.position.x,
                this.isHorizental ? obj.transform.position.y : objSize * idx, obj.transform.position.z);
            c.name = string.Format("{0}_{1}", obj.name, idx);
            source = c;
        });

        Destroy(obj.GetComponent<Renderer>());
    }

    void RepositionBackgroundObjects(GameObject obj)
    {
        var transforms = obj.GetComponentsInChildren<Transform>().Where(t => t.gameObject != obj);
        if (!transforms.Any()) return;

        var first = transforms.First().gameObject;
        var last = transforms.Last().gameObject;
        var objHalfSize = this.isHorizental ? last.GetComponent<Renderer>().bounds.extents.x
            : last.GetComponent<Renderer>().bounds.extents.y;
        var screenHalfSize = this.isHorizental ? this.screenBounds.x : this.screenBounds.y;

        switch (this.loopDirection)
        {
            case LoopDirection.Horizental:
                if (this.transform.position.x + screenHalfSize > last.transform.position.x + (screenHalfSize > objHalfSize ? 0 : objHalfSize - screenHalfSize))
                {
                    first.transform.SetAsLastSibling();
                    first.transform.position = new Vector3(last.transform.position.x + objHalfSize * 2, last.transform.position.y, last.transform.position.z);
                }
                else if (this.transform.position.x - screenHalfSize < first.transform.position.x - (screenHalfSize > objHalfSize ? 0 : objHalfSize - screenHalfSize))
                {
                    last.transform.SetAsFirstSibling();
                    last.transform.position = new Vector3(first.transform.position.x - objHalfSize * 2, first.transform.position.y, first.transform.position.z);
                }
                break;
            case LoopDirection.Vertical:
                if (this.transform.position.y + screenHalfSize > last.transform.position.y + (screenHalfSize > objHalfSize ? 0 : objHalfSize - screenHalfSize))
                {
                    first.transform.SetAsLastSibling();
                    first.transform.position = new Vector3(last.transform.position.x, last.transform.position.y + objHalfSize * 2, last.transform.position.z);
                }
                else if (this.transform.position.y - screenHalfSize < first.transform.position.y - (screenHalfSize > objHalfSize ? 0 : objHalfSize - screenHalfSize))
                {
                    last.transform.SetAsFirstSibling();
                    last.transform.position = new Vector3(first.transform.position.x, first.transform.position.y - objHalfSize * 2, first.transform.position.z);
                }
                break;
        }
        
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        this.gameObjects.ToList().ForEach(obj => this.RepositionBackgroundObjects(obj));
    }
}
